#!/bin/sh
export HOSTNAME=`hostname`
if test ! -r /opt/alice/setup.sh ; then 
    echo "Cannot find ALICE setup script" 
    exit 1
fi
. /opt/alice/setup.sh

dir=$1 
if test "x$dir" = "x" ; then 
    echo "No directory specified" 
    exit 1
fi
if test ! -d $dir ; then 
    mkdir -p $dir
fi

user=proofd
host=hehi00.nbi.dk

# Wait a while 
sleep 3
# printenv  | sort -u
# which root.exe 
(cd $dir 
    if test -f /etc/proof/style.css ; then 
	cp /etc/proof/style.css . 
    fi
    l=`(root.exe -l -b  <<EOF
gEnv->SetValue("Proof.Sandbox", "/data/proof/sandbox/$user");
Printf("Proof.Sandbox=%s", gEnv->GetValue("Proof.Sandbox", "-"));
TProof* proof = TProof::Open("$host");
if (!proof) exit(1);
proof->ShowDataSets();

.q
EOF
) | sed -n '/Dataset URI/,$ p' | sed -e '1 d' | sed 's/[ |].*//' | tr '\n' ' ' | tr ' ' ':'` 
    echo ""
    echo "list is $l"

    root.exe -l -b -q /etc/proof/ExtractDataSets.C\(\".\",\"$l\"\)
    retval=$?
    if test $retval -ne 0 ; then 
	echo "ROOT script failed: $retval"
	exit $retval
    fi
)

exit 0


