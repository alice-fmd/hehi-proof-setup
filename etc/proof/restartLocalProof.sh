#!/bin/sh

doService()
{
    daemon=$1 
    oper=$2 
    
    service $daemon $oper
    
    ret=$? 
    if test $ret -ne 0 ; then 
	echo "service $daemon $oper failed" 
	exit 1
    fi
}

doService xrootd stop 
doService cmsd stop 
doService cmsd start 
doService xrootd start 
doService xrootd status
doService cmsd status 

#
# EOF
#



