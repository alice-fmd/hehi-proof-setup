#!/bin/sh

script="/etc/proof/statusLocalProof.sh"

hostmess()
{
    h=$1 
    echo "Executing $script on $h"
}

for i in `seq 1 5` ; do 
    host=`printf hehi%02d.hehi.nbi.dk $i` 
    hostmess $host
    ssh -q -t $host ${script} 

    ret=$? 
    if test $ret -ne 0 ; then 
	echo "Failed execute $script on $host" 
	exit 1
    fi
done

hostmess Master
${script}

#
# EOF
#
