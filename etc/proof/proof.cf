
# ____________________________________________________________________
# Settings
#
# Set the master host address
set masterHost = hehi00.nbi.dk
# Set the top directory 
set topDir = /data
# Set the proof directory 
set proofDir = /data/proof
# Set temporary directory 
set tmpDir = /tmp
# Set location of ROOT
# if $masterHost 
   # ROOT on master is different then on slaves 
#   set rootLoc = /opt/alice/root/inst-unstable
#else 
#   set rootLoc = /opt/alice/root/inst-14.04
#fi
set rootLoc = /opt/inst/root

# ____________________________________________________________________
# Data access 
#

# --- XRD port -------------------------------------------------------
xrd.port 1094

# --- Administration path --------------------------------------------
all.adminpath $proofDir/admin group

# --- MD5 sums -------------------------------------------------------
# For some reason the built-in MD5 sum algorithm doesn't work on the 
# master, so we resort to a program - should be the same.
# if $masterHost
#     xrootd.chksum max 4 md5 /usr/bin/md5sum
# else
#     xrootd.chksum md5
# fi

# --- File system access ---------------------------------------------
xrootd.fslib /usr/lib/libXrdOfs.so
if $masterHost 
  # requests on master should be redirected to leaves
  ofs.forward all 
fi


# --- Data path to be exported to clients ----------------------------
all.export $topDir 
all.export $tmpDir

# --- Cluster setup --------------------------------------------------
if $masterHost 
     all.role manager     
else 
     all.role server
fi

# --- Where's the manager --------------------------------------------
all.manager $masterHost:1213

# --- Hosts allowed to connect ---------------------------------------
# allow all hehi* machines to connect
cms.allow host *.hehi.nbi.dk
cms.allow host $masterHost
cms.allow host localhost
cms.allow host *.local

# ____________________________________________________________________
# PROOF
#
# All xpd directorives
#
#   trace		[-]keyword [[-]keyword]*
#   groupfile		local-path
#   multiuser		*
#   maxoldlogs		number
#   allow		host
#   allowedgroups	group[,group]*
#   allowedusers	user[,user]*
#   role		[worker|masker|submaster|any]
#   cron		this		Class
#   port		this		Class
#   datadir		this		Class
#   datasetsrc		this		Class
#   rootd		this		Class
#   rootdallow		this		Class
#   xrd.protocol	this		Class
#   filterlibpaths	this		Class
#   tmp		        fTMPdir		String
#   poolurl		fPoolURL	String
#   namespace		fNamespace	String
#   superusers		fSuperUsers	String
#   image		fImage		String
#   workdir		fWorkDir	String
#   sockpathdir         fSockPathDir	String
# --- Load protocol --------------------------------------------------
if exec xrootd 
  xrd.protocol xproofd $rootLoc/lib/libXrdProofd.so
fi
xpd.port 1093

# --- Define which ROOT to use ---------------------------------------
xpd.rootsys $rootLoc

xpd.exportpath $topDir,$tmpDir
xpd.tmp $proofDir/tmp

# --- Allow RootD to run ---------------------------------------------
# xpd.rootd allow mode:rw

# --- User sandboxes -------------------------------------------------
xpd.workdir $proofDir/sandbox
#if $masterHost 
#   xpd.multiuser 1 <workdir>/users/<user>
#fi
# --- Resource finder ------------------------------------------------
# NB: 'if <pattern>' not supported for this directive.
# "static", i.e. using a config file
#   <cfg_file>          path alternative config file
#                       [$ROOTSYS/proof/etc/proof.conf]
#   <user_cfg_opt>      if "yes": enable user private config files at
#                       $HOME/.proof.conf or $HOME/.<usr_def>, where
#                       <usr_cfg> is the second argument to
#                       TProof::Open("<master>","<usr_cfg>") ["no"]
#   <max_workers>       Maximum number of workers to be assigned to user
#                       session [-1, i.e. all]
#   <selection_mode>    If <max_workers> != -1, specify the way workers
#                       are chosen:
#                       "roundrobin"  round-robin selection in bunches
#                                     of n(=<max_workers>) workers.
#                                     Example:
#                                     N = 10 (available workers), n = 4:
#                                     1st (session): 1-4, 2nd: 5-8,
#                                     3rd: 9,10,1,2, 4th: 3-6, ...
#                         "random"    random choice (a worker is not
#                                     assigned twice)
# xpd.resource static [<cfg_file>] [ucfg:<user_cfg_opt>]  [wmx:<max_workers>] 
#     [selopt:<selection_mode>]
# xpd.resource static /etc/proof/proof.conf wmx:-1 selopt:roundrobin

# --- Alternate way of specifying the cluster ------------------------
#
# xpd.worker master $masterHost
# xpd.worker worker $masterHost repeat=4
# xpd.worker worker hehi0[1-9].hehi.nbi.dk repeat=8
# xpd.worker worker hehi01.hehi.nbi.dk repeat=8
xpd.worker worker hehi01.hehi.nbi.dk repeat=4
xpd.worker worker hehi02.hehi.nbi.dk repeat=4  
xpd.worker worker hehi03.hehi.nbi.dk repeat=4 
xpd.worker worker hehi04.hehi.nbi.dk repeat=4
xpd.worker worker hehi05.hehi.nbi.dk repeat=4
xpd.worker worker hehi06.hehi.nbi.dk repeat=4
#xpd.worker worker hehi07.hehi.nbi.dk repeat=4  - not set up 
xpd.worker worker hehi08.hehi.nbi.dk repeat=8
xpd.worker worker hehi09.hehi.nbi.dk repeat=4

# if $masterHost 
#     xpd.bonjour discover servicetype=_xproofd._tcp domain=hehi.nbi.dk 
# else if hehi09*
#    xpd.bonjour register servicetype=_xproofd._tcp domain=hehi.nbi.dk cores=8
# else 
#    xpd.bonjour register servicetype=_xproofd._tcp domain=hehi.nbi.dk cores=4
# fi


# --- Roles ----------------------------------------------------------
# xpd.role worker
# xpd.role master if $masterHost
if $masterHost 
   xpd.role master
else 
   xpd.role worker
fi

# --- Masters allowed to connect -------------------------------------
xpd.allow $masterHost

# --- URL and namespace for local storage ----------------------------
# Note, that this means all data must be accessed as 
# root://$masterHost/$topDir 
xpd.poolurl root://$masterHost
xpd.namespace $topDir

# --- Where to store data sets generated on the cluster 
# xpd.putrc ProofServ.DataDir: root://$masterHost/$topDir/users/<user>
#
# This _must_ be a local directory
xpd.datadir $proofDir/users

# ____________________________________________________________________
# Scheduling 
#
xpd.putrc Proof.DynamicStartup 1

# Queue sessions if needed. 
# Run at max 2 sessions at a time 
# Maximum of 40 workers per session
# Select workers based on load 
# Assign at max 75% of the free CPUs 
# Optimum number of workers/node is 4
# Least number of workers/session is 3
xpd.schedparam queue:fifo mxrun:2 wmx:40 selopt:load fraction:0.75 optnwrks:4 minforquery: 3
 
# ____________________________________________________________________
# User and group management 
#
# xpd.groupfile /etc/proof/groups

# --- Groups allowed (UNIX/PROOF) ------------------------------------
xpd.allowedgroups proofd,hehi,default

# ____________________________________________________________________
# Data sets
# 
# --- Source of data set info ----------------------------------------
# xpd.datasetsrc file url:root://$masterHost/$proofDir/datasets rw:1
xpd.datasetsrc file url:$proofDir/datasets rw:1
xpd.datasetdir $proofDir/datasets


# ____________________________________________________________________
# ROOT RC stuff 
#
# --- Location of merged files ---------------------------------------
xpd.putrc Proof.OutputFile: root://$masterHost/$proofDir/users/<group>/<user>/

# --- Create directories if needed -----------------------------------
xpd.putrc XNet.Mkpath: 1
xpd.putrc Unix.*.Root.DynamicPath: .:$rootLoc/lib
xpd.putrc Rint.Logon: /etc/proof/rootlogon.C
xpd.putrc Rint.Logoff: /etc/proot/rootlogoff.C

# --- Data set manager -----------------------------------------------
# xpd.putrc Proof.GroupFile: /etc/proof/groups

# --- Concurrent reads -----------------------------------------------
# Allow at most 16 processes to read from the same data node at 
# once (default:2)
xpd.putrc Packetizer.MaxWorkersPerNode 3

# --- Dynamic star-up ------------------------------------------------
# This does not seem to work well with PAR files.  In particular, the 
# slaves are not turned on until we do the actual TProof::Process so 
# they cannot recieve the PAR files and unpack them.
# xpd.putrc Proof.DynamicStartup: true

# --- Debug ----------------------------------------------------------
# xpd.putrc Root.Debug: 5
# xpd.putrc Proof.Debug: 5
# xpd.putrc Proof.DebugLevel: 5
# xpd.putrc Proof.DebugMask: 56 
# 0xFFFFFFFF
# xpd.putrc XProof.Debug: 2
# xpd.putrc XNet.Debug: 1
# all.trace all
# xpd.trace all

#
# EOF
#


