#!/bin/sh

doService()
{
    daemon=$1 
    oper=$2 
    
    /usr/sbin/service $daemon $oper
    
    ret=$? 
    if test $ret -ne 0 ; then 
	echo "service $daemon $oper failed" 
	exit 1
    fi
}

doService xrootd status
doService cmsd status 

#
# EOF
#



