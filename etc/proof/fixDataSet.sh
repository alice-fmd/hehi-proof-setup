#!/bin/sh 

hosts="hehi01 hehi02 hehi03 hehi04 hehi05" 
user=setup
# src=/default/cholm/LHC10h_ESD_dispIP_positiveField
src=/hehi/alex/LHC10h_pass2_flowConfig_jan2012
# dest=LHC10h_ESD_displacedVtx_p5k
dest=LHC10h_pass2_AOD_flowconfig_jan2012 
pat=AliESDs*.root

rm -f list

# --- Usage help ----------------------------------------------------
usage()
{
    cat <<-EOF
	Usage: $0 [OPTIONS]

	Options:
		-h,--help			This help
		-u,--user    USER		Set user name [$user]
		-s,--src     OLD_DATASET	Source data set name [$src]
		-d,--dest    NEW_DATASET	New data set name [$dest]
		-p,--pattern PATTERN		Search pattern [$pat]
	
	EOF
}

# --- Command line options ------------------------------------------
while test $# -gt 0 ; do 
    case $1 in 
    -h|--help) usage ; exit 0;;
    -u|--user) user=$2 ; shift ;; 
    -s|--src)  src=$2 ; shift ;;
    -d|--dest) dest=$2 ; shift ;;
    -p|--pattern) pat=$2 ; shift ;;
    *) echo "$0: Unknown option $1" ; exit 1 ;;
    esac
    shift
done

# --- Loop over hosts and generate list -----------------------------
for i in $hosts ; do 
    l=`ssh ${user}@${i}.hehi.nbi.dk \
	find /data/users${src} -name "$pat" 2>/dev/null` 
    for j in $l ; do 
	# echo "root://${i}.hehi.nbi.dk/$j" 
	echo "root://${i}.hehi.nbi.dk/$j" >> list
    done
done 

cat<<EOF > reg.C
void
reg()
{
  TFileCollection* fc = new TFileCollection;
  fc->AddFromFile("list");

  TList* l = fc->GetList();
  TIter next(l);
  TFileInfo* i = 0;
  while ((i = static_cast<TFileInfo*>(next()))) {
    TUrl* u = new TUrl(*i->GetFirstUrl());
    u->SetHost("hehi00.nbi.dk");
    i->AddUrl(u->GetUrl());
  }
  fc->Print("FL");
  TProof::Open("hehi00.nbi.dk");
  gProof->RegisterDataSet("$dest", fc, "VO");
}

EOF

root -b -q -l reg.C

rm -f reg.C 
