#!/bin/sh

script="/etc/proof/restartLocalProof.sh"

hostmess()
{
    h=$1 
    echo "Executing $script on $h"
}

for i in `seq 1 5` ; do 
    host=`printf hehi%02d.hehi.nbi.dk $i` 
    hostmess $host
    # scp -q ${script} ${host}:~/
    ssh -q -t $host sudo ${script}

    ret=$? 
    if test $ret -ne 0 ; then 
	echo "Failed execute $script on $host" 
	exit 1
    fi
done

hostmess Master
sudo ${script}

#
# EOF
#
